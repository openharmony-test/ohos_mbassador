## v2.0.0
1.适配DevEco Studio:3.1 Beta2(3.1.0.400), SDK:API9 Release(3.2.11.9)
2.包管理工具由npm切换成ohpm

## v0.1.1
适配
- [DevEco Studio](https://developer.harmonyos.com/cn/develop/deveco-studio#download) 版本：DevEco Studio 3.1 Beta1
- OpenHarmony SDK版本：API version 9

## v0.1.0
支持特性：
- 支持事件发布，订阅
- 支持同步异步发送事件
- 支持事件过滤
- 支持设置接收事件的强弱引用
- 支持事件注解驱动

