## MBassador

## Introduction

MBassador is a third-party library that utilizes a publish-subscribe pattern to provide the following features:

- Publish and subscribe to events.
- Send events synchronously or asynchronously.
- Filter events.
- Set strong and weak references for received events.
- Implement annotation-driven event handling.

## How to Install

```
ohpm install @ohos/mbassador
```
For details about the OpenHarmony ohpm environment configuration, see [Installing the OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md).

## Annotations

| Annotation|  Description|
|------|------|
| @Handler | Defines the received event handler.|
| @Listener | Defines the listener scope, such as the reference type to use.|

## Properties

### @Handler

| Property|  Description |
|------|------|
| filters?: IMessageFilter\<Object>[] | Whether to filter events.|
| delivery?: Invoke | Whether to publish the event synchronously and asynchronously.|
| priority?: number | Event priority.|
| enabled?: boolean | Whether the event is received.|

### @Listener

| Property|  Description |
|------|------|
| className| Name of the received event object.|
| References | Strong and weak references.|

## How to Use

### Listener

```
import { Annotations, References } from '@ohos/mbassador'
......

@Annotations.Listener("ExampleListener",References.Weak)
export class ExampleListener {
  @Annotations.handle()
  firstMessage(str: string, listener: ExampleListener) {
    //do something
  }

  @Annotations.handle({priority:100000,enabled:false})
  secondMessage(str: string, listener: ExampleListener) {
    //do something
  }

  @Annotations.handle({filters:[new ExampleFilter],enabled:true})
  thirdMessage(str: string, listener: ExampleListener) {
    //do something
  }
}
```

### Sending an Event Synchronously

```
    let mbassador = new MBassador<String>()
    let listener = new ExampleListener(callback)
    mbassador.subscribe(listener);
    mbassador.post(new String("this is first")).now();
```

### Sending an Event Asynchronously

```
    let mbassador:MBassador = new MBassador()
    let listener:ExampleListener = new ExampleListener(callback)
    mbassador.subscribe(listener);
    mbassador.post(new String("this is asynchronously message")).asynchronously();
```

### Unsubscribing

```
   mbassador.unSubscribe(listener);
```

### Filter

```
import { IMessageFilter, SubscriptionContext } from '@ohos/mbassador'

export class ExampleFilter implements IMessageFilter<String> {
    accepts(msg: String, context: SubscriptionContext) {
          //some code
           return true;
    }
}
```
## Directory Structure

```
|---- Mbassador
      |----mbassador 
           |----src
                |---- bus
                |---- config
                |---- dispatch
                |---- entry
                |---- interface
                |---- listener
                |---- subscription
                |---- utils
      |----entry
           |----src
                |---- main
                      |---- ets
                            |---- entryability
                                  |---- EntryAbility.ts
                            |---- pages
                                  |---- index.ets
```

## Constraints
MBassador has been verified in the following versions:

- DevEco Studio: 4.0 (4.0.3.512), SDK: API10 (4.0.10.9)
- DevEco Studio: 3.1 Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)

## How to Contribute
If you find any problem when using MBassador, submit an [issue](https://gitee.com/openharmony-sig/ohos-mbassador/issues) or a [PR](https://gitee.com/openharmony-sig/ohos-mbassador/pulls) to us.


## License
The ohos_coap library is based on [MIT License](https://gitee.com/openharmony-sig/ohos_mbassador/blob/master/LICENSE).
