/*
 * Copyright (C) 2022-2024 Huawei Device Co., Ltd.
 * Licensed under the MIT License, (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://opensource.org/licenses/MIT
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Feature } from './Feature'
import { MetadataReader } from '../listener/MetadataReader'
import { MessagePublicationFactory } from '../bus/MessagePublication'
import { SubscriptionFactory } from '../subscription/SubscriptionFactory'
import type { ISubscriptionManagerProvider } from '../subscription/ISubscriptionManagerProvider'
import { SubscriptionManagerProvider } from '../subscription/SubscriptionManagerProvider'

export class SyncPubSub extends Feature {
    private publicationFactory: MessagePublicationFactory;
    private subscriptionFactory: SubscriptionFactory;
    private subscriptionManagerProvider: ISubscriptionManagerProvider;

    public static Default(): SyncPubSub {
        return new SyncPubSub()
            .setPublicationFactory(new MessagePublicationFactory())
            .setSubscriptionFactory(new SubscriptionFactory())
            .setSubscriptionManagerProvider(new SubscriptionManagerProvider());
    }

    getMetadataReader(): MetadataReader {
        return MetadataReader.getInstance();
    }

    setPublicationFactory(factory: MessagePublicationFactory): SyncPubSub {
        this.publicationFactory = factory;
        return this;
    }

    setSubscriptionFactory(subFactory: SubscriptionFactory): SyncPubSub {
        this.subscriptionFactory = subFactory;
        return this;
    }

    setSubscriptionManagerProvider(subProvider: ISubscriptionManagerProvider): SyncPubSub {
        this.subscriptionManagerProvider = subProvider;
        return this;
    }

    getSubscriptionFactory(): SubscriptionFactory {
        return this.subscriptionFactory;
    }

    getSubscriptionManagerProvider(): ISubscriptionManagerProvider {
        return this.subscriptionManagerProvider;
    }

    getPublicationFactory(): MessagePublicationFactory {
        return this.publicationFactory;
    }
}